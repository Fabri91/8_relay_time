import serial
import time
from datetime import datetime
import schedule
import pickle
import tkinter as tk

LARGE_FONT = ("Verdana", 36)
MED_FONT = ("Verdana", 20)

ser=serial.Serial(port = 'COM3', baudrate = 9600, timeout = 0) #PC
##ser=serial.Serial(port = '/dev/ttyUSB0', baudrate = 9600, timeout = 0) #RaspberryPI

relayAmount = 8
feedAmount = 12
relayStatus = "00000000"

minList = list(range(1,60+1))
minList = list(map(str, minList))
hList = list(range(1,24+1))
hList = list(map(str, hList))

enable = [1]*relayAmount

try:
    f = open('times.pckl', 'rb')
    times = pickle.load(f)
    f.close()
except:
    times = [[[0] * 2 for _ in range(feedAmount)] for _ in range(relayAmount)]  #Start and stop times for each activation for each relay
    f = open('times.pckl', 'wb') #Indices: 0-7 relay select | 0-11 feeding event | 0-1 Start-stop
    pickle.dump(times, f)
    f.close()

class ControlPanel(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        container = tk.Frame(self)

        self.geometry("480x320")
        # self.attributes("-fullscreen", True)

        container.pack(side = "top", fill = "both", expand = True)

        container.grid_rowconfigure(0, weight = 1)
        container.grid_columnconfigure(0, weight = 1)

        self.timevar = tk.StringVar()        

        self.frames = {}
        self.frames["StartPage"] = StartPage(container, self)
        self.frames["StartPage"].grid(row = 0, column = 0, sticky="nsew")    



        for p in list(range(1,relayAmount+1)):
            numString = str(p)
            pageName = "PageRelay" + numString
            self.frames[pageName] = PageRelay(container, self, numString)
            self.frames[pageName].grid(row = 0, column = 0, sticky="nsew")


        for p in list(range(1,relayAmount+1)):
            for q in list (range(1,feedAmount+1)):
                numString = str(p)+ "_" +str(q)
                pageName = "PageTimer" + numString
                self.frames[pageName] = PageTimer(container, self, str(p), str(q))
                self.frames[pageName].grid(row = 0, column = 0, sticky="nsew")


        print(self.frames)

        self.show_frame("StartPage")
        self.recurring()

    def show_frame(self, page_name):
        frame=self.frames[page_name]
        frame.tkraise()

            
    def recurring(self):
        LT = time.strftime('%H:%M')
        LTs = time.strftime('%H:%M:%S')
        self.timevar.set(LTs)
        now = datetime.now()
        seconds = (now - now.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()

        checkRelays(round(seconds))

        serialRead = ser.readline()
        serialRead = serialRead[0 : len(serialRead)-2]
        serialRead = serialRead.decode('utf-8')

        global relayStatus

        if len(serialRead) > 0:
            relayStatus = serialRead
            print(1)
            print(relayStatus)
            
        for p in list(range(0, relayAmount)):
            if relayStatus[p] == '1':
                getattr(self.frames["StartPage"], "colourer" + str(p+1))('#5BB85D')

            if relayStatus[p] == '0':
                getattr(self.frames["StartPage"], "colourer" + str(p+1))('#F0F0F0')
        

##        print(now)
##        print(seconds)
##        print(LT)
##        print(hoursMinsToSeconds(LT))
        
        self.after(500, self.recurring)

def toggleOn(string):
    stringToSend = string + "ON"
    print(stringToSend)
    ser.write(str.encode(stringToSend))

def toggleOff(string):
    stringToSend = string + "OFF"
    print(stringToSend)
    ser.write(str.encode(stringToSend))

def hoursMinsToSeconds(HMstring):
    hours = int(HMstring[0:2])
    minutes = int(HMstring[3:5])
    seconds = hours*3600 + minutes*60
    return seconds

def setTime(hVar, minVar, relayNumber, eventNumber, startStop):
    startTime = hVar.get().zfill(2) + ":" +minVar.get().zfill(2) # fill to guarantee HH:MM format (e.g. 01:09 instead of 1:9)
    #print(startTime)
    startTimeSeconds = hoursMinsToSeconds(startTime)
    #print(times[relayNumber-1][eventNumber-1][startStop])
    times[relayNumber-1][eventNumber-1][startStop] = startTimeSeconds
    #print(times[relayNumber-1][eventNumber-1][startStop])
    f = open('times.pckl', 'wb') #Indices: 0-7 relay select | 0-11 feeding event | 0-1 Start-stop
    pickle.dump(times, f)
    f.close()

def checkRelays(secondsElapsed):
    for p in list(range(0,relayAmount)):
        relayNumber=str(p+1)
        for q in list(range(0,feedAmount)):
            timeOn=times[p][q][0]
            timeOff=times[p][q][1]
            if timeOn != timeOff:
                if secondsElapsed == timeOn and enable[p] == 1:
                    toggleOn(relayNumber)
                if secondsElapsed == timeOff:
                    toggleOff(relayNumber)

def setEnable(relayNumber):
    if enable[relayNumber - 1] == 1:
        enable[relayNumber - 1] = 0
    elif enable[relayNumber - 1] == 0:
        enable[relayNumber - 1] = 1
    

class StartPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)
        label = tk.Label(self, text = "Start Page", font = LARGE_FONT)
        label.grid(column = 0, row = 0, columnspan = 2)

        time = tk.Label(self, textvariable = controller.timevar, font = MED_FONT)
        time.grid(column = 2, row = 0)

        self.btn1 = tk.Button(self, text = "RELAY 1", command=lambda: controller.show_frame("PageRelay1"), font=MED_FONT)
        self.btn2 = tk.Button(self, text = "RELAY 2", command=lambda: controller.show_frame("PageRelay2"), font=MED_FONT)
        self.btn3 = tk.Button(self, text = "RELAY 3", command=lambda: controller.show_frame("PageRelay3"), font=MED_FONT)
        self.btn4 = tk.Button(self, text = "RELAY 4", command=lambda: controller.show_frame("PageRelay4"), font=MED_FONT)
        self.btn5 = tk.Button(self, text = "RELAY 5", command=lambda: controller.show_frame("PageRelay5"), font=MED_FONT)
        self.btn6 = tk.Button(self, text = "RELAY 6", command=lambda: controller.show_frame("PageRelay6"), font=MED_FONT)
        self.btn7 = tk.Button(self, text = "RELAY 7", command=lambda: controller.show_frame("PageRelay7"), font=MED_FONT)
        self.btn8 = tk.Button(self, text = "RELAY 8", command=lambda: controller.show_frame("PageRelay8"), font=MED_FONT)

        quitbtn=tk.Button(self, text="Quit", command=parent.master.destroy, font=MED_FONT)
        
        self.btn1.grid(column = 0, row = 1)
        self.btn2.grid(column = 1, row = 1)
        self.btn3.grid(column = 2, row = 1)
        self.btn4.grid(column = 0, row = 2)
        self.btn5.grid(column = 1, row = 2)
        self.btn6.grid(column = 2, row = 2)
        self.btn7.grid(column = 0, row = 3)
        self.btn8.grid(column = 1, row = 3)
        quitbtn.grid(column = 0, row = 4)


    def colourer1(self, color):
        self.btn1.configure(bg = color)

    def colourer2(self, color):
        self.btn2.configure(bg = color)

    def colourer3(self, color):
        self.btn3.configure(bg = color)

    def colourer4(self, color):
        self.btn4.configure(bg = color)

    def colourer5(self, color):
        self.btn5.configure(bg = color)

    def colourer6(self, color):
        self.btn6.configure(bg = color)

    def colourer7(self, color):
        self.btn7.configure(bg = color)

    def colourer8(self, color):
        self.btn8.configure(bg = color)

class PageRelay(tk.Frame):
    def __init__(self, parent, controller, relayNumber):
        tk.Frame.__init__(self, parent)

        pageTimerString = "PageTimer" + relayNumber
        self.checkValue = tk.IntVar()
        self.checkValue.set(enable[int(relayNumber) - 1])
        enableCheck = tk.Checkbutton(self, text = "Enabled", command=lambda: setEnable(int(relayNumber)), font = MED_FONT, variable = self.checkValue)
        titleLabel = tk.Label(self, text = "Relay " + relayNumber, font=MED_FONT)
        btnTimer1 = tk.Button(self, text = "Timer 1", command=lambda: controller.show_frame(pageTimerString + "_1"), font=MED_FONT)
        btnTimer2 = tk.Button(self, text = "Timer 2", command=lambda: controller.show_frame(pageTimerString + "_2"), font=MED_FONT)
        btnTimer3 = tk.Button(self, text = "Timer 3", command=lambda: controller.show_frame(pageTimerString + "_3"), font=MED_FONT)
        btnTimer4 = tk.Button(self, text = "Timer 4", command=lambda: controller.show_frame(pageTimerString + "_4"), font=MED_FONT)
        btnTimer5 = tk.Button(self, text = "Timer 5", command=lambda: controller.show_frame(pageTimerString + "_5"), font=MED_FONT)
        btnTimer6 = tk.Button(self, text = "Timer 6", command=lambda: controller.show_frame(pageTimerString + "_6"), font=MED_FONT)
        btnTimer7 = tk.Button(self, text = "Timer 7", command=lambda: controller.show_frame(pageTimerString + "_7"), font=MED_FONT)
        btnTimer8 = tk.Button(self, text = "Timer 8", command=lambda: controller.show_frame(pageTimerString + "_8"), font=MED_FONT)
        btnTimer9 = tk.Button(self, text = "Timer 9", command=lambda: controller.show_frame(pageTimerString + "_9"), font=MED_FONT)
        btnTimer10 = tk.Button(self, text = "Timer 10", command=lambda: controller.show_frame(pageTimerString + "_10"), font=MED_FONT)
        btnTimer11 = tk.Button(self, text = "Timer 11", command=lambda: controller.show_frame(pageTimerString + "_11"), font=MED_FONT)
        btnTimer12 = tk.Button(self, text = "Timer 12", command=lambda: controller.show_frame(pageTimerString + "_12"), font=MED_FONT)
        btnOn = tk.Button(self, text = "ON", command=lambda: toggleOn(relayNumber), font=MED_FONT)
        btnOff = tk.Button(self, text = "OFF", command=lambda: toggleOff(relayNumber), font=MED_FONT)
        btnBack = tk.Button(self, text = "BACK", command=lambda: controller.show_frame("StartPage"), font=MED_FONT)

        titleLabel.grid(column = 0, row = 0)
        enableCheck.grid(column = 1, row = 0)
        btnTimer1.grid(column = 0, row = 1)
        btnTimer2.grid(column = 1, row = 1)
        btnTimer3.grid(column = 2, row = 1)
        btnTimer4.grid(column = 0, row = 2)
        btnTimer5.grid(column = 1, row = 2)
        btnTimer6.grid(column = 2, row = 2)
        btnTimer7.grid(column = 0, row = 3)
        btnTimer8.grid(column = 1, row = 3)
        btnTimer9.grid(column = 2, row = 3)
        btnTimer10.grid(column = 0, row = 4)
        btnTimer11.grid(column = 1, row = 4)
        btnTimer12.grid(column = 2, row = 4)
        btnOn.grid(column = 1, row = 5)
        btnOff.grid(column = 2, row = 5)
        btnBack.grid(column = 0, row = 5)

    
class PageTimer(tk.Frame):
    def __init__(self, parent, controller, relayNumber, timerNumber):
        tk.Frame.__init__(self, parent)

        sStartActual = times[int(relayNumber)-1][int(timerNumber)-1][0]
        minStartActual, sStartActual = divmod(sStartActual, 60)
        hStartActual, minStartActual = divmod(minStartActual, 60)
        
        sStopActual = times[int(relayNumber)-1][int(timerNumber)-1][1]
        minStopActual, sStopActual = divmod(sStopActual, 60)
        hStopActual, minStopActual = divmod(minStopActual, 60)

        titleLabel = tk.Label(self, text = "Relay " + relayNumber + ", Timer " + timerNumber, font=MED_FONT)
        labelStart = tk.Label(self, text = "Start: ", font = MED_FONT)
        hStart = tk.Spinbox(self, from_=0, to=23, width = 3, font = MED_FONT)
        hStart.delete(0,"end")
        hStart.insert(0, str(hStartActual))
        minStart = tk.Spinbox(self, from_=0, to=59, width = 3, font = MED_FONT)
        minStart.delete(0,"end")
        minStart.insert(0, str(minStartActual))
        btnStart = tk.Button(self, text = "OK", font = MED_FONT, command=lambda: setTime(hStart, minStart, int(relayNumber), int(timerNumber), 0))

        labelStop = tk.Label(self, text = "Stop: ", font = MED_FONT)
        hStop = tk.Spinbox(self, from_=0, to=23, width = 3, font = MED_FONT)
        hStop.delete(0,"end")
        hStop.insert(0, str(hStopActual))
        minStop = tk.Spinbox(self, from_=0, to=59, width = 3, font = MED_FONT)
        minStop.delete(0,"end")
        minStop.insert(0, str(minStopActual))
        btnStop = tk.Button(self, text = "OK", font = MED_FONT, command=lambda: setTime(hStop, minStop, int(relayNumber), int(timerNumber), 1))

        btnBack = tk.Button(self, text = "BACK", font = MED_FONT, command=lambda: controller.show_frame("PageRelay" + relayNumber))

        titleLabel.grid(column = 0, row = 0)
        labelStart.grid(column = 0, row = 1)
        hStart.grid(column = 1, row = 1)
        minStart.grid(column = 2, row = 1)
        btnStart.grid(column = 3, row = 1)
        labelStop.grid(column = 0, row = 2)
        hStop.grid(column = 1, row = 2)
        minStop.grid(column = 2, row = 2)
        btnStop.grid(column = 3, row = 2
                     )
        btnBack.grid(column = 0, row = 3)
        
        
app=ControlPanel()
app.mainloop()
