import serial
import time
import schedule
import tkinter as tk

LARGE_FONT = ("Verdana", 36)
MED_FONT = ("Verdana", 24)

ser=serial.Serial(port = 'COM3', baudrate = 9600)

timeInit = "00:00"

times = [timeInit, timeInit, timeInit, timeInit, timeInit, timeInit, timeInit, timeInit, timeInit, timeInit, timeInit, timeInit, timeInit, timeInit, timeInit, timeInit]

class ControlPanel(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        container = tk.Frame(self)

        container.pack(side = "top", fill = "both", expand = True)

        container.grid_rowconfigure(0, weight = 1)
        container.grid_columnconfigure(0, weight = 1)

        self.timevar = tk.StringVar()
        self.time1ON = tk.StringVar()
        self.time1OFF = tk.StringVar()
        self.time2ON = tk.StringVar()
        self.time2OFF = tk.StringVar()
        self.time3ON = tk.StringVar()
        self.time3OFF = tk.StringVar()
        self.time4ON = tk.StringVar()
        self.time4OFF = tk.StringVar()
        self.time5ON = tk.StringVar()
        self.time5OFF = tk.StringVar()
        self.time6ON = tk.StringVar()
        self.time6OFF = tk.StringVar()
        self.time7ON = tk.StringVar()
        self.time7OFF = tk.StringVar()
        self.time8ON = tk.StringVar()
        self.time8OFF = tk.StringVar()
        

        self.frames = {}

        for F in (StartPage, Page1, Page2, Page3, Page4, Page5, Page6, Page7, Page8):

            frame = F(container, self)

            self.frames[F] = frame

            frame.grid(row = 0, column = 0, sticky="nsew")

        self.show_frame(StartPage)
        self.recurring()

    def show_frame(self, cont):
        frame=self.frames[cont]
        frame.tkraise()

    def recurring(self):
        LT = time.strftime('%H:%M')
        LTs = time.strftime('%H:%M:%S')
        self.timevar.set(LTs)

        self.time1ON.set(times[0])
        self.time1OFF.set(times[1])

        self.time2ON.set(times[2])
        self.time2OFF.set(times[3])
        
        self.time3ON.set(times[4])
        self.time3OFF.set(times[5])
        
        self.time4ON.set(times[6])
        self.time4OFF.set(times[7])
        
        self.time5ON.set(times[8])
        self.time5OFF.set(times[9])
        
        self.time6ON.set(times[10])
        self.time6OFF.set(times[11])
        
        self.time7ON.set(times[12])
        self.time7OFF.set(times[13])
        
        self.time8ON.set(times[14])
        self.time8OFF.set(times[15])
        
##        print(LT)
##        print(times)

        checkRelay(LT, "1")
        checkRelay(LT, "2")
        checkRelay(LT, "3")
        checkRelay(LT, "4")
        checkRelay(LT, "5")
        checkRelay(LT, "6")
        checkRelay(LT, "7")
        checkRelay(LT, "8")
       
        
        self.after(200, self.recurring)
    
def toggleON(string):
    stringToSend = string + "ON"
    print(stringToSend)
    ser.write(str.encode(stringToSend))

def toggleOFF(string):
    stringToSend = string + "OFF"
    print(stringToSend)
    ser.write(str.encode(stringToSend))

def checkRelay(LT, relayNumber):
    timeON=times[2*(int(relayNumber)-1)]
    timeOFF=times[2*(int(relayNumber)-1)+1]
    if timeON != timeOFF:
        if LT == timeON:
            toggleON(relayNumber)
        if LT == timeOFF:
            toggleOFF(relayNumber)

def setStart(entryName, relayNumber):
    startTime = entryName.get()
    print(startTime)
    times[2*(int(relayNumber)-1)] = startTime


def setStop(entryName, relayNumber):
    stopTime = entryName.get()
    print(stopTime)
    times[2*(int(relayNumber)-1)+1]=stopTime

    
class StartPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)
        label = tk.Label(self, text = "Start Page", font = LARGE_FONT)
        label.grid(column = 0, row = 0, columnspan = 2)

        global times
        

        clock = tk.Label(self, font = LARGE_FONT)
        clock.grid(column = 1, row = 0)

        button1 = tk.Button(self, text="Relay 1", font = LARGE_FONT, command=lambda: controller.show_frame(Page1))
        button2 = tk.Button(self, text="Relay 2", font = LARGE_FONT, command=lambda: controller.show_frame(Page2))
        button3 = tk.Button(self, text="Relay 3", font = LARGE_FONT, command=lambda: controller.show_frame(Page3))
        button4 = tk.Button(self, text="Relay 4", font = LARGE_FONT, command=lambda: controller.show_frame(Page4))
        button5 = tk.Button(self, text="Relay 5", font = LARGE_FONT, command=lambda: controller.show_frame(Page5))
        button6 = tk.Button(self, text="Relay 6", font = LARGE_FONT, command=lambda: controller.show_frame(Page6))
        button7 = tk.Button(self, text="Relay 7", font = LARGE_FONT, command=lambda: controller.show_frame(Page7))
        button8 = tk.Button(self, text="Relay 8", font = LARGE_FONT, command=lambda: controller.show_frame(Page8))

        button1.grid(column = 0, row = 1)
        button2.grid(column = 1, row = 1)
        button3.grid(column = 2, row = 1)
        button4.grid(column = 3, row = 1)
        button5.grid(column = 0, row = 2)
        button6.grid(column = 1, row = 2)
        button7.grid(column = 2, row = 2)
        button8.grid(column = 3, row = 2)

        R1 = tk.Label(self, text = "R1", font = MED_FONT)
        R2 = tk.Label(self, text = "R2", font = MED_FONT)
        R3 = tk.Label(self, text = "R3", font = MED_FONT)
        R4 = tk.Label(self, text = "R4", font = MED_FONT)
        R5 = tk.Label(self, text = "R5", font = MED_FONT)
        R6 = tk.Label(self, text = "R6", font = MED_FONT)
        R7 = tk.Label(self, text = "R7", font = MED_FONT)
        R8 = tk.Label(self, text = "R8", font = MED_FONT)

        R1.grid(column = 0, row = 3)
        R2.grid(column = 1, row = 3)
        R3.grid(column = 2, row = 3)
        R4.grid(column = 3, row = 3)
        R5.grid(column = 0, row = 6)
        R6.grid(column = 1, row = 6)
        R7.grid(column = 2, row = 6)
        R8.grid(column = 3, row = 6)

        def timeOnString(relayNumber):
            return times[2*(int(relayNumber)-1)]

        def timeOffString(relayNumber):
            return times[2*(int(relayNumber)-1)+1]
            
##        timeOn1 = tk.Label(self, text = timeOnString("1"), font = MED_FONT)
        timeOn1 = tk.Label(self, textvariable = controller.time1ON, font = MED_FONT)
        timeOn2 = tk.Label(self, textvariable = controller.time2ON, font = MED_FONT)
        timeOn3 = tk.Label(self, textvariable = controller.time3ON, font = MED_FONT)
        timeOn4 = tk.Label(self, textvariable = controller.time4ON, font = MED_FONT)
        timeOn5 = tk.Label(self, textvariable = controller.time5ON, font = MED_FONT)
        timeOn6 = tk.Label(self, textvariable = controller.time6ON, font = MED_FONT)
        timeOn7 = tk.Label(self, textvariable = controller.time7ON, font = MED_FONT)
        timeOn8 = tk.Label(self, textvariable = controller.time8ON, font = MED_FONT)


        timeOn1.grid(column = 0, row = 4)
        timeOn2.grid(column = 1, row = 4)
        timeOn3.grid(column = 2, row = 4)
        timeOn4.grid(column = 3, row = 4)
        timeOn5.grid(column = 0, row = 7)
        timeOn6.grid(column = 1, row = 7)
        timeOn7.grid(column = 2, row = 7)
        timeOn8.grid(column = 3, row = 7)

##        timeOff1 = tk.Label(self, text = timeOffString("1"), font = MED_FONT)
        timeOff1 = tk.Label(self, textvariable = controller.time1OFF, font = MED_FONT)
        timeOff2 = tk.Label(self, textvariable = controller.time2OFF, font = MED_FONT)
        timeOff3 = tk.Label(self, textvariable = controller.time3OFF, font = MED_FONT)
        timeOff4 = tk.Label(self, textvariable = controller.time4OFF, font = MED_FONT)
        timeOff5 = tk.Label(self, textvariable = controller.time5OFF, font = MED_FONT)
        timeOff6 = tk.Label(self, textvariable = controller.time6OFF, font = MED_FONT)
        timeOff7 = tk.Label(self, textvariable = controller.time7OFF, font = MED_FONT)
        timeOff8 = tk.Label(self, textvariable = controller.time8OFF, font = MED_FONT)


        timeOff1.grid(column = 0, row = 5)
        timeOff2.grid(column = 1, row = 5)
        timeOff3.grid(column = 2, row = 5)
        timeOff4.grid(column = 3, row = 5)
        timeOff5.grid(column = 0, row = 8)
        timeOff6.grid(column = 1, row = 8)
        timeOff7.grid(column = 2, row = 8)
        timeOff8.grid(column = 3, row = 8)

        time = tk.Label(self, textvariable=controller.timevar, font=LARGE_FONT)
        time.grid(row = 0, column = 3)
            




class Page1(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        
        relayNumber = "1"
        
        label = tk.Label(self, text = "Relay " + relayNumber, font = LARGE_FONT)
        
        #timeON = tk.Label(self, font = LARGE_FONT, text=times[2*(int(relayNumber)-1)])
        labelON = tk.Label(self, font = LARGE_FONT, text="ON")
        
        entryON = tk.Entry(self, font = LARGE_FONT)

        buttonConfirmON = tk.Button(self, text="OK", font = LARGE_FONT, command=lambda: setStart(entryON, relayNumber))

        #timeOFF = tk.Label(self, font = LARGE_FONT, text=times[2*(int(relayNumber)-1)+1])
        labelOFF = tk.Label(self, font = LARGE_FONT, text="OFF")
        
        entryOFF = tk.Entry(self, font = LARGE_FONT)

        buttonConfirmOFF = tk.Button(self, text="OK", font = LARGE_FONT, command=lambda: setStop(entryOFF, relayNumber))

        button = tk.Button(self, text="Back", font = LARGE_FONT,
                            command=lambda: controller.show_frame(StartPage))

        buttonON = tk.Button(self, text=relayNumber + " ON", font = LARGE_FONT,
                            command=lambda: toggleON(relayNumber))

        buttonOFF = tk.Button(self, text=relayNumber + " OFF", font = LARGE_FONT,
                            command=lambda: toggleOFF(relayNumber))


        label.grid(column = 0, row = 0)
        #timeON.grid(column = 0, row = 1)
        labelON.grid(column = 0, row = 1)
        entryON.grid(column = 1, row = 1)
        buttonConfirmON.grid(column = 2, row = 1)
        #timeOFF.grid(column = 0, row = 2)
        labelOFF.grid(column = 0, row = 2)
        entryOFF.grid(column = 1, row = 2)
        buttonConfirmOFF.grid(column = 2, row = 2)
        button.grid(column = 0, row = 4)
        buttonON.grid(column = 1, row = 4)
        buttonOFF.grid(column = 2, row = 4)
        

class Page2(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        
        relayNumber = "2"
        
        label = tk.Label(self, text = "Relay " + relayNumber, font = LARGE_FONT)
        
        #timeON = tk.Label(self, font = LARGE_FONT, text=times[2*(int(relayNumber)-1)])
        labelON = tk.Label(self, font = LARGE_FONT, text="ON")
        
        entryON = tk.Entry(self, font = LARGE_FONT)

        buttonConfirmON = tk.Button(self, text="OK", font = LARGE_FONT, command=lambda: setStart(entryON, relayNumber))

        #timeOFF = tk.Label(self, font = LARGE_FONT, text=times[2*(int(relayNumber)-1)+1])
        labelOFF = tk.Label(self, font = LARGE_FONT, text="OFF")
        
        entryOFF = tk.Entry(self, font = LARGE_FONT)

        buttonConfirmOFF = tk.Button(self, text="OK", font = LARGE_FONT, command=lambda: setStop(entryOFF, relayNumber))

        button = tk.Button(self, text="Back", font = LARGE_FONT,
                            command=lambda: controller.show_frame(StartPage))

        buttonON = tk.Button(self, text=relayNumber + " ON", font = LARGE_FONT,
                            command=lambda: toggleON(relayNumber))

        buttonOFF = tk.Button(self, text=relayNumber + " OFF", font = LARGE_FONT,
                            command=lambda: toggleOFF(relayNumber))


        label.grid(column = 0, row = 0)
        #timeON.grid(column = 0, row = 1)
        labelON.grid(column = 0, row = 1)
        entryON.grid(column = 1, row = 1)
        buttonConfirmON.grid(column = 2, row = 1)
        #timeOFF.grid(column = 0, row = 2)
        labelOFF.grid(column = 0, row = 2)
        entryOFF.grid(column = 1, row = 2)
        buttonConfirmOFF.grid(column = 2, row = 2)
        button.grid(column = 0, row = 4)
        buttonON.grid(column = 1, row = 4)
        buttonOFF.grid(column = 2, row = 4)

class Page3(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        
        relayNumber = "3"
        
        label = tk.Label(self, text = "Relay " + relayNumber, font = LARGE_FONT)
        
        #timeON = tk.Label(self, font = LARGE_FONT, text=times[2*(int(relayNumber)-1)])
        labelON = tk.Label(self, font = LARGE_FONT, text="ON")
        
        entryON = tk.Entry(self, font = LARGE_FONT)

        buttonConfirmON = tk.Button(self, text="OK", font = LARGE_FONT, command=lambda: setStart(entryON, relayNumber))

        #timeOFF = tk.Label(self, font = LARGE_FONT, text=times[2*(int(relayNumber)-1)+1])
        labelOFF = tk.Label(self, font = LARGE_FONT, text="OFF")
        
        entryOFF = tk.Entry(self, font = LARGE_FONT)

        buttonConfirmOFF = tk.Button(self, text="OK", font = LARGE_FONT, command=lambda: setStop(entryOFF, relayNumber))

        button = tk.Button(self, text="Back", font = LARGE_FONT,
                            command=lambda: controller.show_frame(StartPage))

        buttonON = tk.Button(self, text=relayNumber + " ON", font = LARGE_FONT,
                            command=lambda: toggleON(relayNumber))

        buttonOFF = tk.Button(self, text=relayNumber + " OFF", font = LARGE_FONT,
                            command=lambda: toggleOFF(relayNumber))


        label.grid(column = 0, row = 0)
        #timeON.grid(column = 0, row = 1)
        labelON.grid(column = 0, row = 1)
        entryON.grid(column = 1, row = 1)
        buttonConfirmON.grid(column = 2, row = 1)
        #timeOFF.grid(column = 0, row = 2)
        labelOFF.grid(column = 0, row = 2)
        entryOFF.grid(column = 1, row = 2)
        buttonConfirmOFF.grid(column = 2, row = 2)
        button.grid(column = 0, row = 4)
        buttonON.grid(column = 1, row = 4)
        buttonOFF.grid(column = 2, row = 4)

class Page4(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        
        relayNumber = "4"
        
        label = tk.Label(self, text = "Relay " + relayNumber, font = LARGE_FONT)
        
        #timeON = tk.Label(self, font = LARGE_FONT, text=times[2*(int(relayNumber)-1)])
        labelON = tk.Label(self, font = LARGE_FONT, text="ON")
        
        entryON = tk.Entry(self, font = LARGE_FONT)

        buttonConfirmON = tk.Button(self, text="OK", font = LARGE_FONT, command=lambda: setStart(entryON, relayNumber))

        #timeOFF = tk.Label(self, font = LARGE_FONT, text=times[2*(int(relayNumber)-1)+1])
        labelOFF = tk.Label(self, font = LARGE_FONT, text="OFF")
        
        entryOFF = tk.Entry(self, font = LARGE_FONT)

        buttonConfirmOFF = tk.Button(self, text="OK", font = LARGE_FONT, command=lambda: setStop(entryOFF, relayNumber))

        button = tk.Button(self, text="Back", font = LARGE_FONT,
                            command=lambda: controller.show_frame(StartPage))

        buttonON = tk.Button(self, text=relayNumber + " ON", font = LARGE_FONT,
                            command=lambda: toggleON(relayNumber))

        buttonOFF = tk.Button(self, text=relayNumber + " OFF", font = LARGE_FONT,
                            command=lambda: toggleOFF(relayNumber))


        label.grid(column = 0, row = 0)
        #timeON.grid(column = 0, row = 1)
        labelON.grid(column = 0, row = 1)
        entryON.grid(column = 1, row = 1)
        buttonConfirmON.grid(column = 2, row = 1)
        #timeOFF.grid(column = 0, row = 2)
        labelOFF.grid(column = 0, row = 2)
        entryOFF.grid(column = 1, row = 2)
        buttonConfirmOFF.grid(column = 2, row = 2)
        button.grid(column = 0, row = 4)
        buttonON.grid(column = 1, row = 4)
        buttonOFF.grid(column = 2, row = 4)

class Page5(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        
        relayNumber = "5"
        
        label = tk.Label(self, text = "Relay " + relayNumber, font = LARGE_FONT)
        
        #timeON = tk.Label(self, font = LARGE_FONT, text=times[2*(int(relayNumber)-1)])
        labelON = tk.Label(self, font = LARGE_FONT, text="ON")
        
        entryON = tk.Entry(self, font = LARGE_FONT)

        buttonConfirmON = tk.Button(self, text="OK", font = LARGE_FONT, command=lambda: setStart(entryON, relayNumber))

        #timeOFF = tk.Label(self, font = LARGE_FONT, text=times[2*(int(relayNumber)-1)+1])
        labelOFF = tk.Label(self, font = LARGE_FONT, text="OFF")
        
        entryOFF = tk.Entry(self, font = LARGE_FONT)

        buttonConfirmOFF = tk.Button(self, text="OK", font = LARGE_FONT, command=lambda: setStop(entryOFF, relayNumber))

        button = tk.Button(self, text="Back", font = LARGE_FONT,
                            command=lambda: controller.show_frame(StartPage))

        buttonON = tk.Button(self, text=relayNumber + " ON", font = LARGE_FONT,
                            command=lambda: toggleON(relayNumber))

        buttonOFF = tk.Button(self, text=relayNumber + " OFF", font = LARGE_FONT,
                            command=lambda: toggleOFF(relayNumber))


        label.grid(column = 0, row = 0)
        #timeON.grid(column = 0, row = 1)
        labelON.grid(column = 0, row = 1)
        entryON.grid(column = 1, row = 1)
        buttonConfirmON.grid(column = 2, row = 1)
        #timeOFF.grid(column = 0, row = 2)
        labelOFF.grid(column = 0, row = 2)
        entryOFF.grid(column = 1, row = 2)
        buttonConfirmOFF.grid(column = 2, row = 2)
        button.grid(column = 0, row = 4)
        buttonON.grid(column = 1, row = 4)
        buttonOFF.grid(column = 2, row = 4)

class Page6(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        
        relayNumber = "6"
        
        label = tk.Label(self, text = "Relay " + relayNumber, font = LARGE_FONT)
        
        #timeON = tk.Label(self, font = LARGE_FONT, text=times[2*(int(relayNumber)-1)])
        labelON = tk.Label(self, font = LARGE_FONT, text="ON")
        
        entryON = tk.Entry(self, font = LARGE_FONT)

        buttonConfirmON = tk.Button(self, text="OK", font = LARGE_FONT, command=lambda: setStart(entryON, relayNumber))

        #timeOFF = tk.Label(self, font = LARGE_FONT, text=times[2*(int(relayNumber)-1)+1])
        labelOFF = tk.Label(self, font = LARGE_FONT, text="OFF")
        
        entryOFF = tk.Entry(self, font = LARGE_FONT)

        buttonConfirmOFF = tk.Button(self, text="OK", font = LARGE_FONT, command=lambda: setStop(entryOFF, relayNumber))

        button = tk.Button(self, text="Back", font = LARGE_FONT,
                            command=lambda: controller.show_frame(StartPage))

        buttonON = tk.Button(self, text=relayNumber + " ON", font = LARGE_FONT,
                            command=lambda: toggleON(relayNumber))

        buttonOFF = tk.Button(self, text=relayNumber + " OFF", font = LARGE_FONT,
                            command=lambda: toggleOFF(relayNumber))


        label.grid(column = 0, row = 0)
        #timeON.grid(column = 0, row = 1)
        labelON.grid(column = 0, row = 1)
        entryON.grid(column = 1, row = 1)
        buttonConfirmON.grid(column = 2, row = 1)
        #timeOFF.grid(column = 0, row = 2)
        labelOFF.grid(column = 0, row = 2)
        entryOFF.grid(column = 1, row = 2)
        buttonConfirmOFF.grid(column = 2, row = 2)
        button.grid(column = 0, row = 4)
        buttonON.grid(column = 1, row = 4)
        buttonOFF.grid(column = 2, row = 4)

class Page7(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        
        relayNumber = "7"
        
        label = tk.Label(self, text = "Relay " + relayNumber, font = LARGE_FONT)
        
        #timeON = tk.Label(self, font = LARGE_FONT, text=times[2*(int(relayNumber)-1)])
        labelON = tk.Label(self, font = LARGE_FONT, text="ON")
        
        entryON = tk.Entry(self, font = LARGE_FONT)

        buttonConfirmON = tk.Button(self, text="OK", font = LARGE_FONT, command=lambda: setStart(entryON, relayNumber))

        #timeOFF = tk.Label(self, font = LARGE_FONT, text=times[2*(int(relayNumber)-1)+1])
        labelOFF = tk.Label(self, font = LARGE_FONT, text="OFF")
        
        entryOFF = tk.Entry(self, font = LARGE_FONT)

        buttonConfirmOFF = tk.Button(self, text="OK", font = LARGE_FONT, command=lambda: setStop(entryOFF, relayNumber))

        button = tk.Button(self, text="Back", font = LARGE_FONT,
                            command=lambda: controller.show_frame(StartPage))

        buttonON = tk.Button(self, text=relayNumber + " ON", font = LARGE_FONT,
                            command=lambda: toggleON(relayNumber))

        buttonOFF = tk.Button(self, text=relayNumber + " OFF", font = LARGE_FONT,
                            command=lambda: toggleOFF(relayNumber))


        label.grid(column = 0, row = 0)
        #timeON.grid(column = 0, row = 1)
        labelON.grid(column = 0, row = 1)
        entryON.grid(column = 1, row = 1)
        buttonConfirmON.grid(column = 2, row = 1)
        #timeOFF.grid(column = 0, row = 2)
        labelOFF.grid(column = 0, row = 2)
        entryOFF.grid(column = 1, row = 2)
        buttonConfirmOFF.grid(column = 2, row = 2)
        button.grid(column = 0, row = 4)
        buttonON.grid(column = 1, row = 4)
        buttonOFF.grid(column = 2, row = 4)

class Page8(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        
        relayNumber = "8"
        
        label = tk.Label(self, text = "Relay " + relayNumber, font = LARGE_FONT)
        
        #timeON = tk.Label(self, font = LARGE_FONT, text=times[2*(int(relayNumber)-1)])
        labelON = tk.Label(self, font = LARGE_FONT, text="ON")
        
        entryON = tk.Entry(self, font = LARGE_FONT)

        buttonConfirmON = tk.Button(self, text="OK", font = LARGE_FONT, command=lambda: setStart(entryON, relayNumber))

        #timeOFF = tk.Label(self, font = LARGE_FONT, text=times[2*(int(relayNumber)-1)+1])
        labelOFF = tk.Label(self, font = LARGE_FONT, text="OFF")
        
        entryOFF = tk.Entry(self, font = LARGE_FONT)

        buttonConfirmOFF = tk.Button(self, text="OK", font = LARGE_FONT, command=lambda: setStop(entryOFF, relayNumber))

        button = tk.Button(self, text="Back", font = LARGE_FONT,
                            command=lambda: controller.show_frame(StartPage))

        buttonON = tk.Button(self, text=relayNumber + " ON", font = LARGE_FONT,
                            command=lambda: toggleON(relayNumber))

        buttonOFF = tk.Button(self, text=relayNumber + " OFF", font = LARGE_FONT,
                            command=lambda: toggleOFF(relayNumber))


        label.grid(column = 0, row = 0)
        #timeON.grid(column = 0, row = 1)
        labelON.grid(column = 0, row = 1)
        entryON.grid(column = 1, row = 1)
        buttonConfirmON.grid(column = 2, row = 1)
        #timeOFF.grid(column = 0, row = 2)
        labelOFF.grid(column = 0, row = 2)
        entryOFF.grid(column = 1, row = 2)
        buttonConfirmOFF.grid(column = 2, row = 2)
        button.grid(column = 0, row = 4)
        buttonON.grid(column = 1, row = 4)
        buttonOFF.grid(column = 2, row = 4)



app=ControlPanel()
app.mainloop()



