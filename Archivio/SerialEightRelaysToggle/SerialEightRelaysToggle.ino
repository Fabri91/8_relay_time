#define RELAY1 4
#define RELAY2 5
#define RELAY3 6
#define RELAY4 7
#define RELAY5 8
#define RELAY6 9
#define RELAY7 10
#define RELAY8 11

char c;
int incomingByte = 0; // for incoming serial data
int state1=0;
int state2=0;
int state3=0;
int state4=0;
int state5=0;
int state6=0;
int state7=0;
int state8=0;

void setup() {
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  pinMode(RELAY1, OUTPUT);
  pinMode(RELAY2, OUTPUT);
  pinMode(RELAY3, OUTPUT);
  pinMode(RELAY4, OUTPUT);
  pinMode(RELAY5, OUTPUT);
  pinMode(RELAY6, OUTPUT);
  pinMode(RELAY7, OUTPUT);
  pinMode(RELAY8, OUTPUT);

  digitalWrite(RELAY1, HIGH);
  digitalWrite(RELAY2, HIGH);
  digitalWrite(RELAY3, HIGH);
  digitalWrite(RELAY4, HIGH);
  digitalWrite(RELAY5, HIGH);
  digitalWrite(RELAY6, HIGH);
  digitalWrite(RELAY7, HIGH);
  digitalWrite(RELAY8, HIGH);
}

void printBack() {
  Serial.println(String(state1)+String(state2)+String(state3)+String(state4)+String(state5)+String(state6)+String(state7)+String(state8));
}

void loop() {
  // send data only when you receive data:
 if (Serial.available() > 0) {
    c = Serial.read();
  
    if (c == '1')
      {
        if (state1 == 0) {digitalWrite(RELAY1,LOW); state1=1; printBack();}
        else if (state1 == 1) {digitalWrite(RELAY1,HIGH); state1=0; printBack();}
        
      }

    if (c == '2')
      {
        if (state2 == 0) {digitalWrite(RELAY2,LOW); state2=1; printBack();}
        else if (state2 == 1) {digitalWrite(RELAY2,HIGH); state2=0; printBack();}
      }

    if (c == '3')
      {
        if (state3 == 0) {digitalWrite(RELAY3,LOW); state3=1; printBack();}
        else if (state3 == 1) {digitalWrite(RELAY3,HIGH); state3=0; printBack();}
      }

    if (c == '4')
      {
        if (state4 == 0) {digitalWrite(RELAY4,LOW); state4=1; printBack();}
        else if (state4 == 1) {digitalWrite(RELAY4,HIGH); state4=0; printBack();}
      }

    if (c == '5')
      {
        if (state5 == 0) {digitalWrite(RELAY5,LOW); state5=1; printBack();}
        else if (state5 == 1) {digitalWrite(RELAY5,HIGH); state5=0; printBack();}
      }

    if (c == '6')
      {
        if (state6 == 0) {digitalWrite(RELAY6,LOW); state6=1; printBack();}
        else if (state6 == 1) {digitalWrite(RELAY6,HIGH); state6=0; printBack();}
      }

    if (c == '7')
      {
        if (state7 == 0) {digitalWrite(RELAY7,LOW); state7=1; printBack();}
        else if (state7 == 1) {digitalWrite(RELAY7,HIGH); state7=0; printBack();}
      }

    if (c == '8')
      {
        if (state8 == 0) {digitalWrite(RELAY8,LOW); state8=1; printBack();}
        else if (state8 == 1) {digitalWrite(RELAY8,HIGH); state8=0; printBack();}
      }
    }
  //Serial.println(String(state1)+String(state2)+String(state3)+String(state4)+String(state5)+String(state6)+String(state7)+String(state8));
  //Serial.println(c);
  //delay(1000);
}
