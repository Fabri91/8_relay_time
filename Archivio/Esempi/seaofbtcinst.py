import serial
import time
from datetime import datetime
import schedule
import tkinter as tk

class SeaofBTCapp(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        container = tk.Frame(self)

        container.pack(side="top", fill="both", expand=True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        database_pages = [1, 2, 3, 4]

        count = 0
        for p in database_pages:
            page_num = p
            frame = Page(container, self)
            self.frames[page_num] = frame
            frame.grid(row=0, column=0, sticky="nsew")
            count += 1

        frame = StartPage(container, self)
        self.frames[count + 1] = frame
        frame.grid(row=0, column=0, sticky='nsew')
        self.show_frame(count + 1)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()

class Page(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        label = tk.Label(self, text="label for Page")
        label.pack()

app=SeaofBTCapp()
app.mainloop()
