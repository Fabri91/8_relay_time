import serial
from tkinter import *

ser=serial.Serial(port='COM3',baudrate=9600)

window = Tk()

window.wm_attributes('-fullscreen','true')

window.title("Relay Control")
lbl = Label(window, text="Hello")
lbl.grid(column=0, row=0)

def quitApp():
	window.destroy()

def clicked1():
	ser.write(str.encode('1'))

def clicked2():
	ser.write(str.encode('2'))

def clicked3():
	ser.write(str.encode('3'))

def clicked4():
	ser.write(str.encode('4'))

def clicked5():
	ser.write(str.encode('5'))

def clicked6():
	ser.write(str.encode('6'))

def clicked7():
	ser.write(str.encode('7'))

def clicked8():
	ser.write(str.encode('8'))

buttonHeight=12
buttonWidth=25
fontSize=50

btn1 = Button(window, text="Relay 1", command=clicked1, font=("Helvetica", fontSize))
btn2 = Button(window, text="Relay 2", command=clicked2, font=("Helvetica", fontSize))
btn3 = Button(window, text="Relay 3", command=clicked3, font=("Helvetica", fontSize))
btn4 = Button(window, text="Relay 4", command=clicked4, font=("Helvetica", fontSize))
btn5 = Button(window, text="Relay 5", command=clicked5, font=("Helvetica", fontSize))
btn6 = Button(window, text="Relay 6", command=clicked6, font=("Helvetica", fontSize))
btn7 = Button(window, text="Relay 7", command=clicked7, font=("Helvetica", fontSize))
btn8 = Button(window, text="Relay 8", command=clicked8, font=("Helvetica", fontSize))

quitbtn=Button(window, text="Quit", command=quitApp, font=("Helvetica", fontSize))

btn1.grid(column=1, row=0)
btn2.grid(column=2, row=0)
btn3.grid(column=3, row=0)
btn4.grid(column=4, row=0)
btn5.grid(column=1, row=1)
btn6.grid(column=2, row=1)
btn7.grid(column=3, row=1)
btn8.grid(column=4, row=1)

quitbtn.grid(column=0, row=1)

window.mainloop()
