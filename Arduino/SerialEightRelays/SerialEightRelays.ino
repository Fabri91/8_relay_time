#define RELAY1 4
#define RELAY2 5
#define RELAY3 6
#define RELAY4 7
#define RELAY5 8
#define RELAY6 9
#define RELAY7 10
#define RELAY8 11

char c;
String strdata;
int incomingByte = 0; // for incoming serial data
int state1=0;
int state2=0;
int state3=0;
int state4=0;
int state5=0;
int state6=0;
int state7=0;
int state8=0;

void setup() {
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  pinMode(RELAY1, OUTPUT);
  pinMode(RELAY2, OUTPUT);
  pinMode(RELAY3, OUTPUT);
  pinMode(RELAY4, OUTPUT);
  pinMode(RELAY5, OUTPUT);
  pinMode(RELAY6, OUTPUT);
  pinMode(RELAY7, OUTPUT);
  pinMode(RELAY8, OUTPUT);

  digitalWrite(RELAY1, HIGH);
  digitalWrite(RELAY2, HIGH);
  digitalWrite(RELAY3, HIGH);
  digitalWrite(RELAY4, HIGH);
  digitalWrite(RELAY5, HIGH);
  digitalWrite(RELAY6, HIGH);
  digitalWrite(RELAY7, HIGH);
  digitalWrite(RELAY8, HIGH);
}

void printBack() {
  Serial.println(String(state1)+String(state2)+String(state3)+String(state4)+String(state5)+String(state6)+String(state7)+String(state8));
}

void loop() {  
  // send data only when you receive data:
  if (Serial.available() > 0) {
    c = Serial.read();

    strdata += c;}
    
  //Serial.println(strdata);
  
  strdata.trim();
  
  if (strdata == "1ON") {digitalWrite(RELAY1,LOW); state1=1; printBack(); strdata = "";}
  else if (strdata == "1OFF") {digitalWrite(RELAY1,HIGH); state1=0; printBack(); strdata = "";}

  if (strdata == "2ON") {digitalWrite(RELAY2,LOW); state2=1; printBack(); strdata = "";}
  else if (strdata == "2OFF") {digitalWrite(RELAY2,HIGH); state2=0; printBack(); strdata = "";}

  if (strdata == "3ON") {digitalWrite(RELAY3,LOW); state3=1; printBack(); strdata = "";}
  else if (strdata == "3OFF") {digitalWrite(RELAY3,HIGH); state3=0; printBack(); strdata = "";}

  if (strdata == "4ON") {digitalWrite(RELAY4,LOW); state4=1; printBack(); strdata = "";}
  else if (strdata == "4OFF") {digitalWrite(RELAY4,HIGH); state4=0; printBack(); strdata = "";}

  if (strdata == "5ON") {digitalWrite(RELAY5,LOW); state5=1; printBack(); strdata = "";}
  else if (strdata == "5OFF") {digitalWrite(RELAY5,HIGH); state5=0; printBack(); strdata = "";}

  if (strdata == "6ON") {digitalWrite(RELAY6,LOW); state6=1; printBack(); strdata = "";}
  else if (strdata == "6OFF") {digitalWrite(RELAY6,HIGH); state6=0; printBack(); strdata = "";}

  if (strdata == "7ON") {digitalWrite(RELAY7,LOW); state7=1; printBack(); strdata = "";}
  else if (strdata == "7OFF") {digitalWrite(RELAY7,HIGH); state7=0; printBack(); strdata = "";}

  if (strdata == "8ON") {digitalWrite(RELAY8,LOW); state8=1; printBack(); strdata = "";}
  else if (strdata == "8OFF") {digitalWrite(RELAY8,HIGH); state8=0; printBack(); strdata = "";}

  //Serial.println(String(state1)+String(state2)+String(state3)+String(state4)+String(state5)+String(state6)+String(state7)+String(state8));
  //Serial.println(c);
  //delay(200);
}
